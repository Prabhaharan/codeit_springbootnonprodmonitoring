<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BETA_springboot</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-08-22T19:19:02</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>10ad4b24-b9e2-4c02-a9a7-e6cddda14838</testSuiteGuid>
   <testCaseLink>
      <guid>77ebaa2f-c0b5-46f7-b54c-e7ff0cf5b606</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SpringBoot/Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0e715b79-714c-4555-bc93-4f97369054f7</id>
         <iterationEntity>
            <iterationType>SPECIFIC</iterationType>
            <value>1</value>
         </iterationEntity>
         <testDataId>Data Files/SpringBootServicesInput/BETA_Services</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>0e715b79-714c-4555-bc93-4f97369054f7</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>url</value>
         <variableId>72178f5a-ce9e-47f2-a36d-396a490c2ea2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0e715b79-714c-4555-bc93-4f97369054f7</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>userName</value>
         <variableId>1772a0e1-9f4c-453e-819d-7ef1eaaeb4f0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0e715b79-714c-4555-bc93-4f97369054f7</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>e5683f8f-d61a-4cee-8832-d412d24a1b62</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>def4d796-0ee3-4e75-9cbb-2dd9fe343d78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SpringBoot/ServicesVerification</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>6dafd806-63a5-40ed-822f-efa56f7c0d8b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/SpringBootServicesInput/BETA_Services</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>6dafd806-63a5-40ed-822f-efa56f7c0d8b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>ServicesKey</value>
         <variableId>b8c7ac47-0881-4ac1-8b54-5dad1edb439e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6dafd806-63a5-40ed-822f-efa56f7c0d8b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>ServicesValue</value>
         <variableId>6e9eb485-12c0-453f-9299-c2739157c986</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6dafd806-63a5-40ed-822f-efa56f7c0d8b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>url</value>
         <variableId>a633a61e-e222-4e5d-aa63-5125af5d7a88</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
