import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.support.FindBy
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.exception.KatalonRuntimeException
import com.kms.katalon.core.exception.StepErrorException
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.relevantcodes.extentreports.ExtentReports
import com.relevantcodes.extentreports.ExtentTest
import com.relevantcodes.extentreports.LogStatus
import org.openqa.selenium.TakesScreenshot
import org.testng.Assert;
import org.testng.annotations.Listeners
import org.testng.annotations.Test
import com.relevantcodes.extentreports.DisplayOrder

ExtentTest test //= GlobalVariable.TestRepo
ExtentReports report1 = GlobalVariable.ExtentRep

try{
Map<String,List<String>> offlineServices = new LinkedHashMap<String,List<String>>()
List<String>  servicesOfflineList=new ArrayList<String>();
WebUI.verifyElementPresent(findTestObject('Object Repository/SpringBootApplications/FilterInput'),5,FailureHandling.STOP_ON_FAILURE)

def key=ServicesKey
def value=ServicesValue
Map<String,String> mapvalue = new HashMap<String,String>()
mapvalue.put(key, value)
//for loop
WebDriver driver = DriverFactory.getWebDriver()
for (Map.Entry<String,String> mapvalue1 : mapvalue.entrySet())
{
GlobalVariable.ServiceName=mapvalue1.getKey()
test = report1.startTest(GlobalVariable.ServiceName+"_"+GlobalVariable.TestCaseName)
GlobalVariable.TestRepo=test
println("Key = " + mapvalue1.getKey() +", Value = " + mapvalue1.getValue())
WebUiCommonHelper.findWebElement(findTestObject('Object Repository/SpringBootApplications/FilterInput'),10).clear()
WebUI.sendKeys(findTestObject('Object Repository/SpringBootApplications/FilterInput'),mapvalue1.getValue(),FailureHandling.STOP_ON_FAILURE)
boolean VerifyServicePresentorNot = WebUI.verifyElementPresent(findTestObject('Object Repository/SpringBootApplications/BellIcon'),2,FailureHandling.OPTIONAL)

WebUI.delay(3)
if(VerifyServicePresentorNot){
TestObject Offline = new TestObject()
Offline.addProperty("xpath", ConditionType.EQUALS, "//span[text()='OFFLINE']",true)
//verify the services offline
boolean verifyoffline=WebUI.verifyElementNotPresent(Offline,3,FailureHandling.OPTIONAL)
if(verifyoffline){
System.out.println(mapvalue1.getValue()+":"+mapvalue1.getKey()+"_Service is UP!!!")
test.log(LogStatus.PASS,"<font color='green'>[<a href="+mapvalue1.getValue()+":"+mapvalue1.getKey()+">"+mapvalue1.getValue()+":"+mapvalue1.getKey()+"</a>]_Service is UP!!!"+"</font>")
}
else{
System.out.println(mapvalue1.getValue()+":"+mapvalue1.getKey()+"service is offline")
test.log(LogStatus.PASS,"<font color='red'>[<a href="+mapvalue1.getValue()+":"+mapvalue1.getKey()+">"+mapvalue1.getValue()+":"+mapvalue1.getKey()+"</a>]_Service is OFFLINE!!!"+"</font>")
}
}
else{
System.out.println(mapvalue1.getValue()+":"+mapvalue1.getKey()+"service is not shown or present in UI!!!")
test.log(LogStatus.PASS,"<font color='red'>[<a href="+mapvalue1.getValue()+":"+mapvalue1.getKey()+">"+mapvalue1.getValue()+":"+mapvalue1.getKey()+"</a>]_Service is not shown or present in UI!!!"+"</font>")
}
}//for loop end

}//try close
catch(Exception message){
	test.log(LogStatus.INFO,"<font color='red'>The Given Service [<a href="+url+">"+url+"</a>]_is Not Responding!!!</font>")
	test.log(LogStatus.FAIL,"<font color='red'>The Given Service [<a href="+url+">"+url+"</a>]_is Not Responding!!!</font>")
	WebDriver driver = DriverFactory.getWebDriver()
	Date d = new Date();
	String screenshotFile = d.toString().replace(":", "_").replace(" ", "_")+ ".png";
	try{
		WebUI.takeScreenshot(System.getProperty("user.dir")+"\\screenshots\\" + screenshotFile)
		test.log(LogStatus.INFO, "Screenshot-> "+ test.addScreenCapture(System.getProperty("user.dir")+"\\screenshots\\"+ screenshotFile));
	}
	catch(Exception e){
			test.log(LogStatus.INFO, "**Unexpected Alert Pop is opened - Cannot take screenshot ");
	}
	
}