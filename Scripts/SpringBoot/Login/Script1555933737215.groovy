import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.exception.KatalonRuntimeException
import com.kms.katalon.core.exception.StepErrorException
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.relevantcodes.extentreports.ExtentReports
import com.relevantcodes.extentreports.ExtentTest
import com.relevantcodes.extentreports.LogStatus

try{

	WebUI.openBrowser("")
	GlobalVariable.ServiceName=url
	WebUI.navigateToUrl(url,FailureHandling.STOP_ON_FAILURE)
	WebUI.delay(1)
	WebUI.maximizeWindow()
	WebUI.setViewPortSize(1920, 1080)
	WebUI.delay(2)
	WebUI.verifyElementPresent(findTestObject('Object Repository/LoginPage/userName'),10,FailureHandling.STOP_ON_FAILURE)
	WebUI.setText(findTestObject('Object Repository/LoginPage/userName'),userName,FailureHandling.STOP_ON_FAILURE)
	WebUI.verifyElementPresent(findTestObject('Object Repository/LoginPage/password'),10,FailureHandling.STOP_ON_FAILURE)
	WebUI.setText(findTestObject('Object Repository/LoginPage/password'),password,FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject('Object Repository/LoginPage/LoginButton'),FailureHandling.STOP_ON_FAILURE)
	WebUI.verifyElementPresent(findTestObject('Object Repository/SpringBootApplications/FilterInput'),10,FailureHandling.STOP_ON_FAILURE)
}catch(StepFailedException e){
 throw e
}
catch(StepErrorException e){
	throw e
  }
catch(KatalonRuntimeException e){
	throw e
  }




