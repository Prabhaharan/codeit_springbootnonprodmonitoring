
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext
import com.relevantcodes.extentreports.DisplayOrder
import com.relevantcodes.extentreports.ExtentReports
import com.relevantcodes.extentreports.ExtentTest
import com.relevantcodes.extentreports.LogStatus

class ScriptListener {

	@BeforeTestCase
	def sampleBeforeTestCase(TestCaseContext testCaseContext) {
	def TestCases=""
	def TC_Name=""
	TestCases=testCaseContext.testCaseId.split("/")
	TC_Name=TestCases[TestCases.length-1]
	GlobalVariable.TcName=TC_Name
	def instance= GlobalVariable.ExRep
	if(instance.equals("")){
	ExtentReports report = CustomKeywords.'reports.extentReports.getInstance'()
	GlobalVariable.ExRep=report
	}
	}


	@AfterTestCase
	def sampleAfterTestCase(TestCaseContext testCaseContext) {
	ExtentTest test = GlobalVariable.TcRep
	ExtentReports report=GlobalVariable.ExRep
	report.endTest(test)
	}


	@BeforeTestSuite
	def sampleBeforeTestSuite(TestSuiteContext testSuiteContext) {
	def TestSuites=""
	def TS_Name=""
	TestSuites=testSuiteContext.testSuiteId.split("/")
	TS_Name=TestSuites[TestSuites.length-1]
	GlobalVariable.TsName=TS_Name
	println TS_Name
	}


	@AfterTestSuite
	def sampleAfterTestSuite(TestSuiteContext testSuiteContext) {
	ExtentReports Finalreport=GlobalVariable.ExRep
	Finalreport.flush()
	}
}