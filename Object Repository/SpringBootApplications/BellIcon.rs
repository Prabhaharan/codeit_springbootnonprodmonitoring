<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BellIcon</name>
   <tag></tag>
   <elementGuidId>e14be86b-e224-41a2-b61c-4d9057c1f4e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//i[contains(@class,'bell')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//i[contains(@class,'bell')]</value>
   </webElementProperties>
</WebElementEntity>
